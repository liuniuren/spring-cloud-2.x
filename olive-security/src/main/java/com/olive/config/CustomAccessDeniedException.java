package com.olive.config;

import org.springframework.security.access.AccessDeniedException;

public class CustomAccessDeniedException extends AccessDeniedException {

	private static final long serialVersionUID = 1373191708664957380L;
	
	private Integer code;
	
	private String msg;
	
	 public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public CustomAccessDeniedException(Integer code, String msg) {
	        super(msg);
	        this.code = code;
	    }
}
