package com.olive.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "withe.urls")
public class WitheUrlProperties {
	
    private List<String> authenticationWhiteUrls;
    
    private List<String> authorizationWhiteUrls;

	public List<String> getAuthenticationWhiteUrls() {
		return authenticationWhiteUrls;
	}

	public void setAuthenticationWhiteUrls(List<String> authenticationWhiteUrls) {
		this.authenticationWhiteUrls = authenticationWhiteUrls;
	}

	public List<String> getAuthorizationWhiteUrls() {
		return authorizationWhiteUrls;
	}

	public void setAuthorizationWhiteUrls(List<String> authorizationWhiteUrls) {
		this.authorizationWhiteUrls = authorizationWhiteUrls;
	}

}
