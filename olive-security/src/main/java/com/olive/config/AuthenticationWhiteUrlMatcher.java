package com.olive.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * 认证(authentication) 
 * 通用认证白名单
 */
@Component
public class AuthenticationWhiteUrlMatcher implements ServerWebExchangeMatcher {
	
	@Autowired
	private WitheUrlProperties witheUrlProperties;

	@Override
	public Mono<MatchResult> matches(ServerWebExchange exchange) {
		return isWhiteList(exchange) ? MatchResult.notMatch() : MatchResult.match();
	}

	public boolean isWhiteList(ServerWebExchange exchange) {
		String currentUrl = exchange.getRequest().getPath().value();
		System.out.println("AuthenticationWhiteUrlMatcher==>" + currentUrl);
		List<String> excludePatterns = witheUrlProperties.getAuthenticationWhiteUrls();
		AntPathMatcher antPathMatcher = new AntPathMatcher();
		// 确认是否是免鉴权资源
		boolean isAuthResource = excludePatterns != null
				&& excludePatterns.stream().anyMatch(s -> antPathMatcher.match(s, currentUrl));
		System.out.println("isAuthResource=======>" + isAuthResource);
		return isAuthResource;
	}

}
