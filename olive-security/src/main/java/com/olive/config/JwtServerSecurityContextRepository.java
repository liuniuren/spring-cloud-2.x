package com.olive.config;

import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

@Service
public class JwtServerSecurityContextRepository implements ServerSecurityContextRepository {

	@Override
	public Mono<Void> save(ServerWebExchange exchange, SecurityContext context) {
		return Mono.empty();
	}

	@Override
	public Mono<SecurityContext> load(ServerWebExchange exchange) {
		HttpHeaders header = exchange.getRequest().getHeaders();
        String token = header.getFirst("token");
        String deviceId = header.getFirst("deviceId");
        JwtAuthenticationToken authentication = new JwtAuthenticationToken(token, deviceId);
        SecurityContext securityContext = new SecurityContextImpl(authentication);
        return Mono.just(securityContext);
	}

}
