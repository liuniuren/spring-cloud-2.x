package com.olive.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

/**
 * 授权 (authorization) 通用鉴权白名单
 */
@Component
public class AuthorizationWhiteUrlMatcher implements ServerWebExchangeMatcher {

	@Autowired
	private WitheUrlProperties witheUrlProperties;

	@Override
	public Mono<MatchResult> matches(ServerWebExchange exchange) {
		String currentUrl = exchange.getRequest().getPath().value();
		System.out.println("AuthorizationWhiteUrlMatcher==>" + currentUrl);
		
		List<String> excludePatterns = witheUrlProperties.getAuthorizationWhiteUrls();
		AntPathMatcher antPathMatcher = new AntPathMatcher();
		// 确认是否是免鉴权资源
		boolean isAuthResource = excludePatterns != null
				&& excludePatterns.stream().anyMatch(s -> antPathMatcher.match(s, currentUrl));
		System.out.println("isAuthResource=====>" + isAuthResource);
		
		return isAuthResource ? MatchResult.match() : MatchResult.notMatch();
	}

}
