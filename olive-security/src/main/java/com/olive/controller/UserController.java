package com.olive.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	
	@RequestMapping("/user/list")
	public Map<String, Object> listUser(){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("code", 200);
		result.put("mssage", "success");
		return result;
	}

}
