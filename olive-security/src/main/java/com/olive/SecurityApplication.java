package com.olive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * https://www.cnblogs.com/zyly/p/12286285.html
 * 
 * https://juejin.cn/post/6899849049638338573
 * https://blog.51cto.com/u_15065850/2581716
 * 
 * https://www.mianshigee.com/question/171372igb/
 * 
 */
@SpringBootApplication
public class SecurityApplication {

	public static void main(String[] args) {
		 SpringApplication.run(SecurityApplication.class, args);
	}
	
}